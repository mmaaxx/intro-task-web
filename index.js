function test() {
  var g = new Game();

  console.log(g.move(3));
  console.log(JSON.stringify(g.status()));
  console.log(JSON.stringify(g._field));
  console.log(JSON.stringify(g._history));

  console.log(g.move(6));
  console.log(JSON.stringify(g.status()));
  console.log(JSON.stringify(g._field));
  console.log(JSON.stringify(g._history));

  console.log(g.move(4));
  console.log(JSON.stringify(g.status()));
  console.log(JSON.stringify(g._field));
  console.log(JSON.stringify(g._history));

  console.log(g.move(7));
  console.log(JSON.stringify(g.status()));
  console.log(JSON.stringify(g._field));
  console.log(JSON.stringify(g._history));

  console.log(g.move(2));
  console.log(JSON.stringify(g.status()));
  console.log(JSON.stringify(g._field));
  console.log(JSON.stringify(g._history));

  console.log("--- undo: ", g.undo(), " ---");
  console.log(JSON.stringify(g._history));

  console.log("--- undo: ", g.undo(), " ---");
  console.log(JSON.stringify(g._history));

  console.log("--- redo: ", JSON.stringify(g.redo()), " ---");
  console.log(JSON.stringify(g._history));

  console.log(g.move(8));
  console.log(JSON.stringify(g.status()));
  console.log(JSON.stringify(g._field));
  console.log(JSON.stringify(g._history));

  console.log("--- redo: ", JSON.stringify(g.redo()), " ---");
  console.log(JSON.stringify(g._history));

  var f = new Field();

  f.fillCell(0, "x");
  f.fillCell(3, "x");
  f.fillCell(6, "x");
  f.drawLine([0, 3, 6], "vertical");
  f.removeLine();
  f.reset();
  f.enableUndo(true);
  f.enableUndo(false);
}

function reflectStatusInfo(f, g) {
  f.enableUndo(g.canUndo());
  f.enableRedo(g.canRedo());

  var status = g.status();
  if (status.code === "notOver") {
    f.showWonTitle(false, null);
    f.removeLine();
    return;
  }

  switch (status.code) {
    case "notOver":
      return;
    case "draw":
      f.showWonTitle(true, "It's a draw!");
      return;
    case "xWon":
      f.showWonTitle(true, "Crosses won!");
      f.drawLine(status.cellNums, status.lineDirection);
      return;
    case "oWon":
      f.showWonTitle(true, "Toes won!");
      f.drawLine(status.cellNums, status.lineDirection);
      return;
    default:
      throw "Unknown status code!!!";
  }
}

function main() {
  var g = new Game();
  var f = new Field();

  f.addListener("move", function(cellNum) {
    var moveType = g.move(cellNum);
    if (!moveType) {
      return;
    }
    f.fillCell(cellNum, moveType);
    reflectStatusInfo(f, g);
  });

  f.addListener("undo", function() {
    var cellNum = g.undo();
    f.clearCell(cellNum);
    reflectStatusInfo(f, g);
  });

  f.addListener("redo", function() {
    var moveInfo = g.redo();
    f.fillCell(moveInfo.cellNum, moveInfo.moveType);
    reflectStatusInfo(f, g);
  });

  f.addListener("reset", function() {
    f.reset();
    g = new Game();
    reflectStatusInfo(f, g);
  });
}

main();
