function Field() {
  var self = this;
  this._listeners = [];

  var cells = document.querySelectorAll(".field .cell");
  cells.forEach(function(cell) {
    var id = cell.dataset.id;
    cell.addEventListener("click", function() {
      self._notifyListeners("move", id);
    });
  });

  var undoBtn = document.querySelector(".undo-btn");
  undoBtn.addEventListener("click", function() {
    self._notifyListeners("undo", null);
  });

  var undoBtn = document.querySelector(".redo-btn");
  undoBtn.addEventListener("click", function() {
    self._notifyListeners("redo", null);
  });

  var resetBtn = document.querySelector(".restart-btn");
  resetBtn.addEventListener("click", function() {
    self._notifyListeners("reset", null);
  });
}

Field.prototype.reset = function() {
  var cells = document.querySelectorAll(".field .cell");
  cells.forEach(function(cell) {
    cell.className = "cell";
  });
};

Field.prototype.clearCell = function(cellNum) {
  var cell = this._getCell(cellNum);
  cell.className = "cell";
};

Field.prototype.fillCell = function(cellNum, cellType) {
  var cell = this._getCell(cellNum);
  cell.classList.add(cellType === "x" ? "ch" : "r");
};

Field.prototype.drawLine = function(cellNums, lineDirection) {
  var self = this;
  cellNums.forEach(function(cellNum) {
    var cell = self._getCell(cellNum);
    cell.classList.add("win", lineDirection);
  });
};

Field.prototype.removeLine = function() {
  var cells = document.querySelectorAll(".field .cell");
  cells.forEach(function(cell) {
    var className = "cell";
    if (cell.classList.contains("ch")) {
      className += " ch";
    }
    if (cell.classList.contains("r")) {
      className += " r";
    }
    cell.className = className;
  });
};

Field.prototype.enableUndo = function(enable) {
  var btn = document.querySelector(".undo-btn");
  if (enable) {
    btn.removeAttribute("disabled");
  } else {
    btn.setAttribute("disabled", "disabled");
  }
};

Field.prototype.enableRedo = function(enable) {
  var btn = document.querySelector(".redo-btn");
  if (enable) {
    btn.removeAttribute("disabled");
  } else {
    btn.setAttribute("disabled", "disabled");
  }
};

Field.prototype.showWonTitle = function(show, message) {
  var title = document.querySelector(".won-title");
  var wonMessage = document.querySelector(".won-message");
  if (show) {
    title.classList.remove("hidden");
    wonMessage.innerHTML = message;
  } else {
    title.classList.add("hidden");
    wonMessage.innerHTML = "";
  }
};

Field.prototype.addListener = function(event, func) {
  if (!this._listeners[event]) {
    this._listeners[event] = [];
  }
  this._listeners[event].push(func);
};

Field.prototype._notifyListeners = function(event, data) {
  if (!this._listeners[event]) {
    return;
  }
  this._listeners[event].forEach(function(func) {
    func(data);
  });
};

Field.prototype._getCell = function(cellNum) {
  var id = "c-" + cellNum;
  return document.querySelector("#" + id);
};
