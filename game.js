function Game() {
  this._field = [[null, null, null], [null, null, null], [null, null, null]];
  this._xExpected = true;
  this._status = {
    code: "notOver"
  };
  this._history = [];
  this._curHistoryIndex = -1;
}

Game.prototype.move = function(cellNum) {
  if (this._status.code !== "notOver") {
    return null;
  }
  var res = this._moveWithoutHistory(cellNum);
  this._writeToHistory(cellNum);
  return res;
};

Game.prototype.undo = function() {
  if (!this.canUndo()) {
    return null;
  }
  var cellNum = this._history[this._curHistoryIndex];
  var col = cellNum % 3;
  var row = (cellNum - col) / 3;
  this._curHistoryIndex -= 1;
  this._xExpected = !this._xExpected;
  this._field[row][col] = null;
  this._setStatus();
  return cellNum;
};

Game.prototype.canUndo = function() {
  return this._curHistoryIndex >= 0;
};

Game.prototype.redo = function() {
  if (!this.canRedo()) {
    return null;
  }
  this._curHistoryIndex += 1;
  var cellNum = this._history[this._curHistoryIndex];
  this._moveWithoutHistory(cellNum);

  return {
    cellNum: cellNum,
    moveType: !this._xExpected ? "x" : "o"
  };
};

Game.prototype.canRedo = function() {
  return this._curHistoryIndex < this._history.length - 1;
};

Game.prototype._writeToHistory = function(cellNum) {
  this._curHistoryIndex += 1;
  if (this._curHistoryIndex === this._history.length) {
    this._history.push(cellNum);
    return;
  }
  this._history.splice(this._curHistoryIndex, this._history.length, cellNum);
};

Game.prototype.status = function() {
  return this._status;
};

Game.prototype._moveWithoutHistory = function(cellNum) {
  var col = cellNum % 3;
  var row = (cellNum - col) / 3;
  this._field[row][col] = this._xExpected ? "x" : "o";
  this._xExpected = !this._xExpected;
  this._setStatus();
  return !this._xExpected ? "x" : "o";
};

Game.prototype._setStatus = function() {
  var winInfo = this._findWon();
  if (winInfo) {
    this._status = {
      code: winInfo.winner + "Won",
      lineDirection: winInfo.line,
      cellNums: winInfo.cellNums
    };
    return;
  }
  this._status = {
    code: this._isThereEmptyCell() ? "notOver" : "draw"
  };
};

Game.prototype._isThereEmptyCell = function() {
  return !!this._field.find(function(row) {
    return row.includes(null);
  });
};

Game.prototype._findWon = function() {
  return (
    this._findWonRow() ||
    this._findWonCol() ||
    this._findWonRightDiagonal() ||
    this._findWonLeftDiagonal()
  );
};

Game.prototype._findWonRow = function() {
  var self = this;
  var rowIndex = this._field.findIndex(function(row) {
    return self._allEqual(row);
  });
  if (rowIndex < 0) {
    return null;
  }
  var row = this._field[rowIndex];
  var cellNums = row.map(function(val, colIndex) {
    return self._indexesToCellNum(rowIndex, colIndex);
  });
  return {
    winner: row[0],
    line: "horizontal",
    cellNums: cellNums
  };
};

Game.prototype._findWonCol = function() {
  var self = this;
  var cols = this._field[0].map(function(val, col) {
    return self._field.map(function(row) {
      return row[col];
    });
  });
  var colIndex = cols.findIndex(function(col) {
    return self._allEqual(col);
  });
  if (colIndex < 0) {
    return null;
  }
  var cellNums = this._field.map(function(row, rowIndex) {
    return self._indexesToCellNum(rowIndex, colIndex);
  });
  return {
    winner: this._field[0][colIndex],
    line: "vertical",
    cellNums: cellNums
  };
};

Game.prototype._findWonRightDiagonal = function() {
  var self = this;
  var diag = this._field.map(function(row, index) {
    return row[index];
  });
  if (!this._allEqual(diag)) {
    return null;
  }
  var cellNums = this._field.map(function(val, rowIndex) {
    return self._indexesToCellNum(rowIndex, rowIndex);
  });
  return {
    winner: this._field[0][0],
    line: "diagonal-right",
    cellNums: cellNums
  };
};

Game.prototype._findWonLeftDiagonal = function() {
  var self = this;
  var diag = this._field.map(function(row, index, arr) {
    return row[arr.length - index - 1];
  });
  if (!this._allEqual(diag)) {
    return null;
  }
  var cellNums = this._field.map(function(row, rowIndex, arr) {
    var colIndex = arr.length - rowIndex - 1;
    return self._indexesToCellNum(rowIndex, colIndex);
  });
  return {
    winner: this._field[0][this._field[0].length - 1],
    line: "diagonal-left",
    cellNums: cellNums
  };
};

Game.prototype._allEqual = function(arr) {
  return !!(
    arr[0] &&
    arr.every(function(item) {
      return item === arr[0];
    })
  );
};

Game.prototype._indexesToCellNum = function(row, col) {
  return row * this._field.length + col;
};
